package com.example.rotfl.kpnv11;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by rotfl on 18.01.2016.
 */
public class MainActivity extends Activity {

    String      nazwaWl;
    String      nrRejestr;
    String      url;
    String      idSamochodu;
    String      idWlasciciela;
    Integer     licznik;

    TextView    nazwaWlasciciela;
    TextView    nrRejestracyjny;
    TextView    iloscZarejestrowana;    //wyswietla ile punktów zostało zapisaych do bazy danych

    LogActivity la = new LogActivity();
    ConfigClass cc = new ConfigClass();
    AsyncHttpClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);


        nazwaWlasciciela = (TextView) findViewById(R.id.nazwaWlasciciela);
        nrRejestracyjny = (TextView) findViewById(R.id.nrRejestracyjny);
        iloscZarejestrowana = (TextView) findViewById(R.id.ilośćZarejestrowana);

        licznik = 0;

        Intent i = getIntent();
        Bundle przekazDane = i.getExtras();
        nazwaWl = przekazDane.getString("nazwaWlasciciela");
        nrRejestr = przekazDane.getString("nrRejestr");
        idSamochodu = przekazDane.getString("idSamochodu");
        idWlasciciela = przekazDane.getString("idWlasciciela");

        nazwaWlasciciela.setText(nazwaWl);
        nrRejestracyjny.setText(nrRejestr);


        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener ll = new mylocationlistener();
        try{
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ll);
        }catch(SecurityException e){
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

//**************************** Po wcisnięciu przycisku fizycznego ustawień *************************
//*************************** Pojawią się możliwości ustawien i wylogowania ************************
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//*************** Obsługa menu tu - przycisk Wyloguj się *******************************************
        if (id == R.id.action_logout) {
            la.insertHistoriaLog(idWlasciciela, idSamochodu, "2");
            Intent i = new Intent(getApplicationContext(), LogActivity.class);
            idSamochodu = "0";
            startActivity(i);
            finish();
        }
//**************************************************************************************************
        return super.onOptionsItemSelected(item);
    }
//**************************************************************************************************


//********************pobieranie długości i szerokości na których się znajdujemy *******************
// *******************         i wypisanie ich na ekrenie telefonu  *******************************
    class mylocationlistener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            if(location !=null){
                double pLong = location.getLongitude();
                double pLat = location.getLatitude();
                float predkosc = location.getSpeed();


                if(Integer.parseInt(idSamochodu)!=0){
                    insertDlSze(Double.toString(pLong), Double.toString(pLat), Float.toString(predkosc));

                }



            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

// Jeśli podczas działania aplikacji zostanie wyłączony  czujnik GPS, wtedy użytkownik zostanie wylogowany
        // a dane z informacjami o samochodzie przestaną być wysyłąne na serwer
        @Override
        public void onProviderDisabled(String provider) {
            statusGPS();
        }
    }
//**************************************************************************************************


//******** Metoda odpowiedzialna z wyświetlanie okna dialogowego z 2 przyciskami *******************
    public void statusGPS(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Twój czujnik GPS nie jest uruchomiony. Musił włączyć czujnik GPS, aby korzystać z aplikacji.")
                .setCancelable(false)
                .setPositiveButton("Ustawienia", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,  final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Wyloguj", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        idSamochodu = "0";
                        Intent i = new Intent(getApplicationContext(), LogActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
//**************************************************************************************************

//******************** Metoda ma za zadanie zapisywać dane do bazy danych **************************
//***************** takich jak długość, szerokość, prędkosc oraz idSamochodu ***********************
    public void insertDlSze(String dlugosc, String szerokosc, String predkosc){


        client = new AsyncHttpClient();
        url = cc.getIpAdress()+"/insertPozycja.php?dlugosc="+dlugosc+"&szerokosc="+szerokosc+"&predkosc="+Double.parseDouble(predkosc)*3.6+"&idsamochodu="+idSamochodu;

        iloscZarejestrowana.setText(Double.parseDouble(predkosc)*3.6 + "");


        final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);
                try {

                    inputStream.read();
                    inputStream.close();
                } catch (IOException e) {
                    System.out.println("wwwwwwwwwwwwwwwwwwwwwwwwwwssssssssss "+ licznik);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
        try {
//                //Jeśli użytkownik będziie zalogowany jego idSamochodu > 0, natomiast jeśli będzie
//                //wylogowany jego idSamochodu będzie ==0
            System.out.println("aktualny czas ");
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
//**************************************************************************************************




}
