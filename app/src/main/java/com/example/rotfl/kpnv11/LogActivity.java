package com.example.rotfl.kpnv11;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.rotfl.kpnv11.AdapterClass.LogowanieAdapter;
import com.example.rotfl.kpnv11.AdapterClass.LogowanieClass;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.apache.http.Header;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * A login screen that offers login via email/password.
 * LogActivity odpowiada za obsługę activity_login.xml
 * Dzięki tej klasie użytkownik może się zalogować na swoje konto
 */
public class LogActivity extends Activity  {

    String                    url;
    String                    bezReklamy;
    ArrayList<LogowanieClass> logowanieClasses;
    LogowanieAdapter          logowanieAdapter;
    ConfigClass               cc    =   new ConfigClass();

    TextView    nRrejestracyjny;
    TextView    haslo;
    Button      zalogujButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nRrejestracyjny = (TextView)    findViewById(R.id.nRrejestr);
        haslo           = (TextView)    findViewById(R.id.haslo);
        zalogujButton   = (Button)      findViewById(R.id.button);



        zalogujButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zaloguj(nRrejestracyjny.getText().toString(), haslo.getText().toString());
            }
        });




//-------------------------------------------------------------------------------------------------
//------------------ Usuwanie napisu NumerRejestracyjny i haslo z EditTextów ----------------------
//-------------------------------------------------------------------------------------------------
        nRrejestracyjny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nRrejestracyjny.setText("");
            }
        });

        haslo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                haslo.setText("");
            }
        });
//-------------------------------------------------------------------------------------------------
    }

//**************************** Zadaniem metody jest sprawdzanie, czy *******************************
//********************* jest możliwe zalogowanie się dalsze do aplikacji ***************************
//************************* Podając NR rejestracyjny samochodu i hasło *****************************
// ******************************** przypisane do uzytkownika **************************************
    public void zaloguj(final String nrRejestracyjny, final String password){


        url = cc.getIpAdress()+"/selectLogowanieNadajnik.php?nrejestracyjny="+nrRejestracyjny+"&haslo="+password;
        System.out.println(url);
        AsyncHttpClient client;
        client = new AsyncHttpClient();
        logowanieClasses = new ArrayList<>();
        final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String text = new String(responseBody);
                utnijReklame(text);

                Gson gson = new Gson();
                com.google.gson.JsonParser jsonParser = new com.google.gson.JsonParser();
                if(bezReklamy !=null){
                    JsonArray jsonArray = jsonParser.parse(bezReklamy).getAsJsonArray();
                    for (JsonElement jsonElement : jsonArray) {
                        LogowanieClass log = gson.fromJson(jsonElement, LogowanieClass.class);
                        logowanieClasses.add(log);
                    }
                    logowanieAdapter = new LogowanieAdapter(getApplicationContext(), logowanieClasses);
                    if(logowanieClasses.size()==1){
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        i.putExtra("idWlasciciela", logowanieClasses.get(0).getIdWlasciciela());
                        i.putExtra("idSamochodu", logowanieClasses.get(0).getIdSamochodu());    //przesyłaie wartosci zmiennej do Main Activity
                        i.putExtra("nazwaWlasciciela", logowanieClasses.get(0).getImie() + " " + logowanieClasses.get(0).getNazwisko());
                        i.putExtra("nrRejestr", logowanieClasses.get(0).getNrRejestracyjny());



    //linijka dodaje dane do tabeli historiaLogowan ------------------------------------------------
                        insertHistoriaLog(logowanieClasses.get(0).getIdWlasciciela(), logowanieClasses.get(0).getIdSamochodu(), "1");
    //-----------------------------------------------------------------------------------------------

                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }
//***************************************************************************************************



//metoda zapisuje do bazy danych WLASCICIELA SAMOCHODU oraz czas logowania do aplikacji

    public void insertHistoriaLog(String idWlasciciela, String idSamochodu, String stan){

        AsyncHttpClient client;
        client = new AsyncHttpClient();
        url = cc.getIpAdress()+"/insertHistoria.php?idWlasciciela="+idWlasciciela+"&idSamochodu="+idSamochodu+"&stan="+stan;
        System.out.println("Historia" + url);

        final RequestHandle requestHandle = client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                InputStream inputStream = new ByteArrayInputStream(responseBody);
                try {
                    inputStream.read();
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            }
        });
    }


    public void utnijReklame(String textResponseBody ){
//-------------------------> W przypadku darmowego serwera  na końcu JSona zapisywana jest wartość
//-------------------------> inna, niepotrzena wartość (śmieci, które trzeba usunąć i tu to się własnie robi.
        int pocztekSmieci = textResponseBody.indexOf("<");
        bezReklamy =  textResponseBody.substring(0,pocztekSmieci);
    }
}


